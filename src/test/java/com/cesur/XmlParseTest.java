package com.cesur;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import java.util.List;
/**
 * Unit test for simple App.
 */
public class XmlParseTest 
{
    /**
     *  Test :-)
     */
    @Test
    public void ComprobarElementosXML()
    {
        XmlParse x =new XmlParse();
        String url ="https://sites.google.com/site/falfiles/Home/archivos/ejemplo.xml";
        List<String>  list = x.leer("Descargas/descarga/titulo/text()",url);
        assertEquals(list.size(), 2);
    }
}
